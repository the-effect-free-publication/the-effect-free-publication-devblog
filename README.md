# The Effect Free Publication Dev Blog

This is the development blog of the effect free publication. This exists to
provide a background and reference as to how the publication came to be and
why.

## References

This project is based on [archie-zola](https://github.com/XXXMrG/archie-zola/)
and [apollo](https://github.com/not-matthias/apollo). The Publication itself is
being built from the ground up, and designed from scratch. Due to this I did
not feel the need to create the actual blog from scratch. However of course,
since the blog is highly specialised I have modified the aforementioned
themes slight, but they look great otherwise!
