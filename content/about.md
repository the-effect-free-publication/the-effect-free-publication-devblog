+++
title = "About"
path = "about"
+++

This is the effect free editorial development blog. Come here for updates and
progress in the making of the editorial.

# Personnel

## Euan Mendoza
