+++
title = "Editorial: Why Another Publication"
date = 2022-10-13
+++

The idea of making yet another newspaper/magazine seems frankly ridiculous.
While not as tricky as other passion projects could be, the challenging
prospects of making one from scratch are blatantly unnecessary and unneeded.
Personal Publications are unheard of and a frankly unwanted idea in the modern
realm of blogging and micro-blogging. 

<!-- more -->

# So why another publication?


While at first glance, it is easy to dismiss the idea of the difference between
a publication and a blog. By nature, a blog implies ideas and presents
assumptions about a writing style. By nature, a blog is limiting. The short and
sweet answer to why another publication is because it is a
**creative endeavour**.

## Addressing a lack of trust in journalism and media and the abuse entailed

An interesting question to ponder, especially before I stepped into the world of
interaction design and when the world seemed one-dimensional and
straightforward, was 'is social media fake?'. It is easy to dismiss media of all
forms, social, narrative, viewable and readable, as fundamentally biased and
flawed. By nature, media is internally conflicting. People search for a source
of realism and truth, while the media seeks to provide opinions and reaffirm
inherent bias.

Before tackling journalistic integrity and media bias, I wanted to make
a case for social media. When one as a *designer* goes to design a
social interaction between users, they are fundamentally built with a
mission and a goal. Good design ideas are created to address problems
and find *creative* and *innovative* solutions how to solve them. But
alas, to every design is attached a corporate agenda, and this corporate
agenda manifests in hideous ways. Look at Facebook, for example. My Dad
has recently discovered the coveted Facebook. After watching my Dad use
the coveted Facebook, it became immediately apparent what Facebook\'s
goal and design choices were. My Dad, an avid writer and storyteller,
often writes long descriptions and stories to go along with the pictures
he posts, painting an emotional portrait of the moment an image was
captured. The name Facebook implies a picture of a face and a story
alongside it. The nuance is that Facebook has a reputation for
neglecting privacy and selling users\' information for monetary value. I
pose the question now, does the inherent negative corporate values and
ethics presented by Facebook as a company reflect on the design choices
and whether they are accomplishing their goal.

Here I widen the question to argue for all media. There is an inherent
distinction between creative endeavours and corporate malice. We often
argue that media does not present opinions but presents advertisements
to readerships. Which is true\... However, I then ask whether this
represents all media or just the ones people actually read. For the
longest time, long-form journalism has been around to tackle this
question. Magazines like the Monthly and the New Yorker challenge what
it means to be journalistic and whether we can present journalism as a
creative endeavour. Some would argue that this kind of journalism is not
for the mass public, to which I would say by nature, newspapers and
magazines are for those who like to read in a world full of easily
consumable media. By extension, the New Yorker is *targeted* at those of
us who like to read.

In essence, from the beginning, my opinion on the importance of
journalism and good writing within journalism is not a rebuttal or to
refute the negative impacts that Journalism has had in the current
climate of influence. The rebuttal is that Journalism is broad. We
cannot narrowly define Journalism as \'The Media\' defined by corporate
motivation. The Effect Free Publication aims to be a free and open
creative platform for pure creative writing and journalism. It aims to
challenge views and readership but at the same time **not** to address
or buy into trends or unnuanced opinions. By virtue of being a passion
project, it aims to elevate thinking of design and creativity through
all modes and aspects.

## Why Creativity is Important

In the same vein as the inherent mistrust of media and the pessimism
towards social media, we have built a society that confuses creativity
and independent thinking. We are often told to make our own decisions,
be independent thinkers and come up with our own solutions to problems.
Creativity, however, does not care about dependence. Creativity is the
act of creating something, and the more unexpected, the better.
Creativity aims to challenge preconceptions and redefine how technology
and tools are used.

I would argue that creativity cannot exist in a fully independent
society. All creative projects and ideas I have seen were built
collaboratively. In interaction design, the fundamental nature of our
creative ideas is that we come up with *solutions* to *other people\'s*
problems. You don\'t simply pluck an idea out of thin air and reward
yourself as a genius.

Circling to my original point, creative ideas change the world and
redefine people\'s understanding of specific ideas. A painting might
challenge a person\'s visual preconceptions and redefine what it means
to understand shapes and colours. The Effect Free Publication is about
striving for the highest quality creativity not backed by corporate
control or oversight. To present serif typography and push graphic
design to the forefront. To reembrace the old in a digital world. An
online publication is much more than a blog because a blog presents
content, while a publication presents an experience.

I would say creativity is in an awkward era of striving to its utmost
potential within the internet. The challenge lies in how easily it\'s
exploited for what it is. Game Developers are happy to be underpaid and
overworked to develop the next creative masterpiece. Designers play
second fiddle to programmers and corporate overlords even though they
are critical in ideation and probably the most important bridge between
the user and the product. Why pay a graphic designer what their art is
worth when you can find someone on Fiverr for an astronomically lower
price to create something equivalent. Bringing us to NFTs, AI-generated
art and what it means for creative professionals. The Effect Free
Publication is not about solving world crises but valuing art where it
becomes a scam. The Effect Free Publication is about challenging
conceptions of how creativity is used. To present well-researched
arguments that challenge the readers and redefine people\'s value of
innovation. Our creativity and innovation value is simply how we reach
out to readers and change their emotions and ideas. If someone
appreciates what we have written and enjoys the typography and the
experience. If graphic design and art push boundaries and force users to
ask questions, then we have accomplished our goal.

